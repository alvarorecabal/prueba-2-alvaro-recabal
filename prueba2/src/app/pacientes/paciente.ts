export class Paciente {
    public nombre: string;
    public apellidoPaterno: string;
    public apellidoMaterno: string;
    public sexo: string;
    public edad: number;
    public correo: string;
    public sistemaSalud: string;
    public id:number;
}
