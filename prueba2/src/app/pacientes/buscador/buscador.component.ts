import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PacienteService } from '../paciente.service';
import { Paciente } from '../paciente';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  pacientes: Paciente[];
  termino: string;
  buscado: any[];

  constructor(private activatedRoute: ActivatedRoute,
              private pacienteService: PacienteService,
              private router: Router) { 
                this.buscado = [];
              }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.termino = params['termino'];
    });
    this.pacienteService.buscarPacientes().subscribe(res => {
      this.pacientes = res;
      //console.log(this.pacientes);
      for (let i = 0; i < this.pacientes.length; i++) {
        let nombre = this.pacientes[i].nombre.toLowerCase();
        let apellidoPaterno = this.pacientes[i].apellidoPaterno.toLowerCase();
        if(nombre.indexOf(this.termino) >= 0 || apellidoPaterno.indexOf(this.termino) >= 0) {
          this.buscado.push(this.pacientes[i]);
        }
      }
    });
  }

  redireccionarAEditar(id) {
    this.router.navigate(['editar-paciente',id]);
  }

  verDetalle(id) {
    this.router.navigate(['ver-detalle',id]);
  }

}
