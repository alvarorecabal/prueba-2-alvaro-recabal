import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PacientesRoutingModule } from './pacientes-routing.module';
import { CrearPacienteComponent } from './crear-paciente/crear-paciente.component';
import { EditarPacienteComponent } from './editar-paciente/editar-paciente.component';
import { ListarPacienteComponent } from './listar-paciente/listar-paciente.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PacienteService } from './paciente.service';
import { VerDetalleComponent } from './ver-detalle/ver-detalle.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { BuscadorComponent } from './buscador/buscador.component'

@NgModule({
  imports: [
    CommonModule,
    PacientesRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  declarations: [CrearPacienteComponent, EditarPacienteComponent, ListarPacienteComponent, VerDetalleComponent, BuscadorComponent],
  providers: [PacienteService]
})
export class PacientesModule { }
