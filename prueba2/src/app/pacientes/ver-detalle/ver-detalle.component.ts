import { Component, OnInit } from '@angular/core';
import { Paciente } from '../paciente';
import { PacienteService } from '../paciente.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-ver-detalle',
  templateUrl: './ver-detalle.component.html',
  styleUrls: ['./ver-detalle.component.css']
})
export class VerDetalleComponent implements OnInit {

  public paciente: Paciente;
  public id: number;


  constructor(private pacienteService: PacienteService,
              private activatedRoute: ActivatedRoute) {
                this.paciente = new Paciente();
               }

  ngOnInit() {
    this.activatedRoute.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.pacienteService.buscarPaciente(this.id).subscribe((paciente) => {
      this.paciente = paciente;
    });
  }

}
