import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearPacienteComponent } from './crear-paciente/crear-paciente.component';
import { AutenticacionGuard } from '../autenticacion/autenticacion.guard';
import { ListarPacienteComponent } from './listar-paciente/listar-paciente.component';
import { EditarPacienteComponent } from './editar-paciente/editar-paciente.component';
import { VerDetalleComponent } from './ver-detalle/ver-detalle.component';
import { BuscadorComponent } from './buscador/buscador.component';

const routes: Routes = [
  {path:'crear-paciente', component:CrearPacienteComponent, canActivate:[AutenticacionGuard]},
  {path:'listado-pacientes', component:ListarPacienteComponent, canActivate:[AutenticacionGuard]},
  {path:'editar-paciente/:id', component:EditarPacienteComponent, canActivate:[AutenticacionGuard]},
  {path:'ver-detalle/:id', component:VerDetalleComponent, canActivate:[AutenticacionGuard]},
  {path:'buscar/:termino', component:BuscadorComponent, canActivate:[AutenticacionGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacientesRoutingModule { }
