import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Paciente } from '../paciente';
import { PacienteService } from '../paciente.service';

@Component({
  selector: 'app-crear-paciente',
  templateUrl: './crear-paciente.component.html',
  styleUrls: ['./crear-paciente.component.css']
})
export class CrearPacienteComponent implements OnInit {

  public sexos: string[];
  public sistemasSalud: string[];
  public formPacientes: FormGroup;
  public paciente: Paciente;
  public nombre: FormControl;
  public apellidoPaterno: FormControl;
  public apellidoMaterno: FormControl;
  public sexo: FormControl;
  public edad: FormControl;
  public correo: FormControl;
  public sistemaSalud: FormControl;

  constructor(private pacienteService: PacienteService) { 
    this.paciente = new Paciente();
    this.nombre = new FormControl('',[Validators.required, Validators.maxLength(20)]);
    this.apellidoPaterno = new FormControl('',[Validators.required, Validators.maxLength(20)]);
    this.apellidoMaterno = new FormControl('',[Validators.required, Validators.maxLength(20)]);
    this.sexo = new FormControl('', Validators.required);
    this.edad = new FormControl('',[Validators.required,Validators.pattern('[0-9]*')]);
    this.correo = new FormControl('',[Validators.required,Validators.pattern('^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$')]);
    this.sistemaSalud = new FormControl('',Validators.required);
    this.sexos = ["Masculino", "Femenino"];
    this.sistemasSalud = ["Fonasa", "Indigente", "Isapre"];
  }

  ngOnInit() {
    this.formPacientes = new FormGroup({
      nombre: this.nombre,
      apellidoPaterno: this.apellidoPaterno,
      apellidoMaterno: this.apellidoMaterno,
      sexo: this.sexo,
      edad: this.edad,
      correo: this.correo,
      sistemaSalud: this.sistemaSalud
    });
  }

  AgregarPacientes() {
    if(this.formPacientes.valid) {
      this.paciente.nombre = this.nombre.value;
      this.paciente.apellidoPaterno = this.apellidoPaterno.value;
      this.paciente.apellidoMaterno = this.apellidoMaterno.value;
      this.paciente.sexo = this.sexo.value;
      this.paciente.edad = this.edad.value;
      this.paciente.correo = this.correo.value;
      this.paciente.sistemaSalud = this.sistemaSalud.value;
      console.log(this.paciente);
      this.pacienteService.guardarPaciente(this.paciente).subscribe(res => {
        this.paciente = res;
        this.formPacientes.reset();
      });
    }
  }

}
