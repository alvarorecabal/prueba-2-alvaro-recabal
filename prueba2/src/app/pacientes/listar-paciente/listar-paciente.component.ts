import { Component, OnInit } from '@angular/core';
import { Paciente } from '../paciente';
import { PacienteService } from '../paciente.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-paciente',
  templateUrl: './listar-paciente.component.html',
  styleUrls: ['./listar-paciente.component.css']
})
export class ListarPacienteComponent implements OnInit {

  public pacientes: Paciente[];
  public termino: string;

  constructor(private pacienteService: PacienteService,
              private router: Router) {
                this.pacientes = [];
                this.termino = '';
               }

  ngOnInit() {
    this.pacienteService.buscarPacientes().subscribe(res => {
      this.pacientes = res;
    })
  }

  redireccionarAEditar(id) {
    this.router.navigate(['editar-paciente',id]);
  }

  verDetalle(id) {
    this.router.navigate(['ver-detalle',id]);
  }

  buscar(termino) {
    this.router.navigate(['/buscar', termino])
  }

}
