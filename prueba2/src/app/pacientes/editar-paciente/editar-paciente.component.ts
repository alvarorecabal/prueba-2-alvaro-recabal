import { Component, OnInit } from '@angular/core';
import { Paciente } from '../paciente';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PacienteService } from '../paciente.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-editar-paciente',
  templateUrl: './editar-paciente.component.html',
  styleUrls: ['./editar-paciente.component.css']
})
export class EditarPacienteComponent implements OnInit {

  public id: number;
  public paciente: Paciente;
  public formEditarPacientes: FormGroup;
  public sexos: string[];
  public sistemasSalud: string[];

  constructor(private pacienteService: PacienteService,
              private activatedRoute: ActivatedRoute) {
                this.formEditarPacientes = new FormGroup({
                  nombre : new FormControl('',[Validators.required, Validators.maxLength(20)]),
                  apellidoPaterno : new FormControl('',[Validators.required, Validators.maxLength(20)]),
                  apellidoMaterno : new FormControl('',[Validators.required, Validators.maxLength(20)]),
                  sexo : new FormControl('', Validators.required),
                  edad : new FormControl('',[Validators.required,Validators.pattern('[0-9]*')]),
                  correo : new FormControl('',[Validators.required,Validators.pattern('^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$')]),
                  sistemaSalud : new FormControl('',Validators.required),
                });
                this.sexos = ["Masculino", "Femenino"];
                this.sistemasSalud = ["Fonasa", "Indigente", "Isapre"];
               }

  ngOnInit() {
    this.activatedRoute.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.pacienteService.buscarPaciente(this.id).subscribe((paciente) => {
      this.paciente = paciente;
      this.formEditarPacientes.controls.nombre.setValue(this.paciente.nombre);
      this.formEditarPacientes.controls.apellidoPaterno.setValue(this.paciente.apellidoPaterno);
      this.formEditarPacientes.controls.apellidoMaterno.setValue(this.paciente.apellidoMaterno);
      this.formEditarPacientes.controls.sexo.setValue(this.paciente.sexo);
      this.formEditarPacientes.controls.edad.setValue(this.paciente.edad);
      this.formEditarPacientes.controls.correo.setValue(this.paciente.correo);
      this.formEditarPacientes.controls.sistemaSalud.setValue(this.paciente.sistemaSalud);
    });
  }

  editarPacientes() {
    if(this.formEditarPacientes.valid) {
      this.paciente.nombre = this.formEditarPacientes.controls.nombre.value;
      this.paciente.apellidoPaterno = this.formEditarPacientes.controls.apellidoPaterno.value;
      this.paciente.apellidoMaterno = this.formEditarPacientes.controls.apellidoMaterno.value;
      this.paciente.sexo = this.formEditarPacientes.controls.sexo.value;
      this.paciente.edad = this.formEditarPacientes.controls.edad.value;
      this.paciente.correo = this.formEditarPacientes.controls.correo.value;
      this.paciente.sistemaSalud = this.formEditarPacientes.controls.sistemaSalud.value;
    }
    this.pacienteService.editarPaciente(this.paciente).subscribe(res => {
      this.formEditarPacientes.reset();
    });
  }

}
