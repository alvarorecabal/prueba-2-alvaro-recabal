import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Paciente } from './paciente';

@Injectable()
export class PacienteService {

  private URL_API = 'http://localhost:3004/pacientes';

  constructor(private httpClient : HttpClient) {}

  buscarPacientes() : Observable<Paciente[]> {
    return this.httpClient.get<Paciente[]>(this.URL_API);
  }

  buscarPaciente(id : Number) : Observable<Paciente> {
    return this.httpClient.get<Paciente>(this.URL_API + '/' + id);
  }

  guardarPaciente(paciente : Paciente) : Observable<Paciente> {
    return this.httpClient.post<Paciente>(this.URL_API, paciente);
  }

  editarPaciente(paciente : Paciente) : Observable<Paciente[]> {
    return this.httpClient.put<Paciente[]>(this.URL_API + '/' + paciente.id, paciente);
  }

}
