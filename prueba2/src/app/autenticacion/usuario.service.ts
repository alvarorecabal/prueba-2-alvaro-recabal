import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Usuario } from './usuario';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UsuarioService {

  private URL_API = 'http://localhost:3004/usuarios';

  constructor(private httpClient: HttpClient) { }

  buscarUsuarios() : Observable<Usuario[]> {
    return this.httpClient.get<Usuario[]>(this.URL_API);
  }

  buscarUsuario(id : Number) : Observable<Usuario> {
    return this.httpClient.get<Usuario>(this.URL_API + '/' + id);
  }

  loginUsuario(usuario : Usuario) {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('password', usuario.password.toString());
    httpParams = httpParams.set('email', usuario.email.toString());
    console.log(httpParams.toString());
    return this.httpClient.get<Usuario>(this.URL_API, {params : httpParams});
  }

}
