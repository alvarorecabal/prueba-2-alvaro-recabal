import { Injectable } from '@angular/core';
import { Usuario } from './usuario';

const KEY_USUARIOS = 'usuarios';

@Injectable()
export class AutenticacionService {

  constructor() { }

  static guardarEnSesion(nombre : string, objeto : string) {
    localStorage.setItem(nombre, objeto);
  }

  static buscarEnSesion(nombre:string) : string {
    return localStorage.getItem(nombre);
  }

  static limpiarSesion() {
    localStorage.clear();
  }

  static guardarUsuarioEnSesion(usuario : Usuario) {
    this.guardarEnSesion(KEY_USUARIOS, JSON.stringify(usuario));
  }

  static buscarUsuarioEnSesion() : Usuario {
    if (null == this.buscarEnSesion(KEY_USUARIOS)){
      return null;
    }
    let usuario = new Usuario();
    usuario = JSON.parse(this.buscarEnSesion(KEY_USUARIOS));
      return usuario;
  }

}
