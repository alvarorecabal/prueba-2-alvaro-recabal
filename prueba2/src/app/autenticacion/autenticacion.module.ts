import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutenticacionRoutingModule } from './autenticacion-routing.module';
import { LoginComponent } from './login/login.component';
import { DatosUsuarioComponent } from './datos-usuario/datos-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AutenticacionService } from './autenticacion.service';
import { UsuarioService } from './usuario.service';
import { AutenticacionGuard } from './autenticacion.guard';

@NgModule({
  imports: [
    CommonModule,
    AutenticacionRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [LoginComponent, DatosUsuarioComponent],
  providers: [AutenticacionService, UsuarioService, AutenticacionGuard],
  exports: [DatosUsuarioComponent]
})
export class AutenticacionModule { }
