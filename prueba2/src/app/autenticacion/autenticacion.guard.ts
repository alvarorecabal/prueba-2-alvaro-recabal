import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AutenticacionService } from './autenticacion.service';

@Injectable()
export class AutenticacionGuard implements CanActivate {
  canActivate(): boolean {
    if(null != AutenticacionService.buscarUsuarioEnSesion()) {
      return true;
    }
    return false;   
  }
}
