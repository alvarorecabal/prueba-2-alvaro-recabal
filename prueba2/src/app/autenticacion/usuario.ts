export class Usuario {
    public nombre: string;
    public apellido: string;
    public email: string;
    public password: string;
    public id: Number;
}
