import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { Usuario } from '../usuario';
import { UsuarioService } from '../usuario.service';
import { Router } from '@angular/router';
import { AutenticacionService } from '../autenticacion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public formLogin: FormGroup;
  public usuario: Usuario;
  public email: FormControl;
  public password: FormControl;

  constructor(private usuarioService: UsuarioService,
              private router: Router) {
    this.email = new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$')
    ]);
    this.password = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.formLogin = new FormGroup({
      email: this.email,
      password: this.password
    });
  }

  iniciarSesion(){
    if(this.formLogin.valid) {
      //console.log(this.email.value);
      let usuario = new Usuario();
      usuario.password = this.password.value;
      usuario.email = this.email.value;
      this.usuarioService.buscarUsuarios().subscribe(res => {
        console.log(res);
        for (let usu of res) {
          if(usu.password == usuario.password && usu.email == usuario.email) {
            this.usuarioService.loginUsuario(usuario).subscribe(
              (val) =>{
                usuario = val;
                console.log(usuario);
                if(usuario != null) {
                  usuario.password = '';
                  AutenticacionService.guardarUsuarioEnSesion(usuario);
                  this.router.navigate(['/crear-paciente']);
                }
              },
            (error) => {
              console.log('error');
              console.log(error);
            });
          }
        }
      });
    }

  }

}
