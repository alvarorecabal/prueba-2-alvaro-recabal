import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario';
import { AutenticacionService } from '../autenticacion.service';

@Component({
  selector: 'app-datos-usuario',
  templateUrl: './datos-usuario.component.html',
  styleUrls: ['./datos-usuario.component.css']
})
export class DatosUsuarioComponent implements OnInit {

  public usuario: Usuario;
  public nombre: string;
  public apellido: string;
  public estado: boolean;

  constructor() {
    this.estado = false;
   }

  ngOnInit() {
    if(this.usuario = AutenticacionService.buscarUsuarioEnSesion()){
      this.nombre = this.usuario[0].nombre;
      this.apellido = this.usuario[0].apellido;
      this.estado = true;
    }
  }

}
