import { Component } from '@angular/core';
import { Usuario } from './autenticacion/usuario';
import { AutenticacionService } from './autenticacion/autenticacion.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  public usuario: Usuario;

  ngDoCheck(){
    this.usuario = AutenticacionService.buscarUsuarioEnSesion();
  }

  cerrarSesion(){
    AutenticacionService.limpiarSesion();
  }
}
