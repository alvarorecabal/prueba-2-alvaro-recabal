import { Component, OnInit } from '@angular/core';
import { Ficha } from '../../ficha-atencion/ficha';
import { FichaService } from '../../ficha-atencion/ficha.service';
import { Router } from '@angular/router';
import { HistorialService } from '../historial.service';

@Component({
  selector: 'app-ver-historial',
  templateUrl: './ver-historial.component.html',
  styleUrls: ['./ver-historial.component.css']
})
export class VerHistorialComponent implements OnInit {

  public fichas: Ficha[];
  public id:Number;

  constructor(private fichaService: FichaService,
              private router: Router) {
    this.fichas = [];
    this.fichas = HistorialService.buscarFichaEnSesion();
    for (let ficha of this.fichas) {
      
    }
    //console.log(this.fichas);
   }

  ngOnInit() {
  }

  verMas(idx) {
    console.log(idx);
    this.router.navigate(['/ver-mas', idx]);
  }

}
