import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutenticacionGuard } from '../autenticacion/autenticacion.guard';
import { VerHistorialComponent } from './ver-historial/ver-historial.component';
import { VerDetalleHistorialComponent } from './ver-detalle-historial/ver-detalle-historial.component';

const routes: Routes = [
  {path:'historial', component:VerHistorialComponent, canActivate:[AutenticacionGuard]},
  {path:'ver-mas/:id', component:VerDetalleHistorialComponent, canActivate:[AutenticacionGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistorialRoutingModule { }
