import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerDetalleHistorialComponent } from './ver-detalle-historial.component';

describe('VerDetalleHistorialComponent', () => {
  let component: VerDetalleHistorialComponent;
  let fixture: ComponentFixture<VerDetalleHistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerDetalleHistorialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerDetalleHistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
