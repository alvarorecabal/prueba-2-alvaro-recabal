import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Ficha } from '../../ficha-atencion/ficha';
import { FichaService } from '../../ficha-atencion/ficha.service';
import { HistorialService } from '../historial.service';

@Component({
  selector: 'app-ver-detalle-historial',
  templateUrl: './ver-detalle-historial.component.html',
  styleUrls: ['./ver-detalle-historial.component.css']
})
export class VerDetalleHistorialComponent implements OnInit {

  public ficha: Ficha;
  public id: number;
  public fichas: any[];

  constructor(private activatedRoute: ActivatedRoute,
              private fichaService: FichaService) {
    this.ficha = new Ficha();
   }

  ngOnInit() {
    this.activatedRoute.params.forEach((params: Params) => {
      this.id = params['id'];
    });
    this.fichas = HistorialService.buscarFichaEnSesion()
    console.log(this.fichas)
    for(let i = 0; i < this.fichas.length; i++) {
      //console.log(i);
      //console.log(this.id)
      if(this.id == i) {
        this.ficha.nombrePaciente = this.fichas[i].nombrePaciente;
        this.ficha.patologia = this.fichas[i].patologia;
        this.ficha.detalle = this.fichas[i].detalle;
        this.ficha.nombreDoctor = this.fichas[i].nombreDoctor
      }
    }
  }

}
