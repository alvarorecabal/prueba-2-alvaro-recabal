import { Injectable } from '@angular/core';
import { Ficha } from '../ficha-atencion/ficha';

const KEY_HISTORIAL = 'historial';

@Injectable()
export class HistorialService {

  constructor() { }

  static guardarFicha(nombre:string, objeto:string){
    localStorage.setItem(nombre, objeto);
  }

  static buscarFicha(nombre:string):string{
      return localStorage.getItem(nombre);
  }

  static guardarFichaEnSesion(producto: Ficha[]){
    this.guardarFicha(KEY_HISTORIAL, JSON.stringify(producto)); 
  }

  static buscarFichaEnSesion():Ficha[]{
    if(null == this.buscarFicha(KEY_HISTORIAL)){
      return null;
    }
    let producto = JSON.parse(this.buscarFicha(KEY_HISTORIAL));
    return producto;
  }

}
