import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistorialRoutingModule } from './historial-routing.module';
import { VerHistorialComponent } from './ver-historial/ver-historial.component';
import { VerDetalleHistorialComponent } from './ver-detalle-historial/ver-detalle-historial.component';

@NgModule({
  imports: [
    CommonModule,
    HistorialRoutingModule
  ],
  declarations: [VerHistorialComponent, VerDetalleHistorialComponent]
})
export class HistorialModule { }
