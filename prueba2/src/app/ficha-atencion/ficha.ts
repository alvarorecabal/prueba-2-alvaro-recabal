export class Ficha {
    nombreDoctor: string;
    nombrePaciente: string[];
    patologia: string;
    detalle: string;
    id: number;
}
