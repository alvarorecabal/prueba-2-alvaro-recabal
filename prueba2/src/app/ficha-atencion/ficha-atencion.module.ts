import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FichaAtencionRoutingModule } from './ficha-atencion-routing.module';
import { FichaAtencionComponent } from './ficha-atencion/ficha-atencion.component';
import { CrearPatologiaComponent } from './crear-patologia/crear-patologia.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AutenticacionService } from '../autenticacion/autenticacion.service';
import { PatologiaService } from './patologia.service';
import { FichaService } from './ficha.service';

@NgModule({
  imports: [
    CommonModule,
    FichaAtencionRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [FichaAtencionComponent, CrearPatologiaComponent],
  providers: [AutenticacionService, PatologiaService, FichaService]
})
export class FichaAtencionModule { }
