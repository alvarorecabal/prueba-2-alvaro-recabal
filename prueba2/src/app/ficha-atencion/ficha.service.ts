import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ficha } from './ficha';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FichaService {

  private URL_API = 'http://localhost:3004/fichas';

  constructor(private httpClient: HttpClient) { }

  buscarFichas() : Observable<Ficha[]> {
    return this.httpClient.get<Ficha[]>(this.URL_API);
  }

  buscarFicha(id : Number) : Observable<Ficha> {
    return this.httpClient.get<Ficha>(this.URL_API + '/' + id);
  }

  guardarFicha(ficha : Ficha) : Observable<Ficha> {
    return this.httpClient.post<Ficha>(this.URL_API, ficha);
  }

}
