import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../autenticacion/usuario';
import { AutenticacionService } from '../../autenticacion/autenticacion.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Paciente } from '../../pacientes/paciente';
import { Patologia } from '../patologia';
import { Ficha } from '../ficha';
import { PacienteService } from '../../pacientes/paciente.service';
import { PatologiaService } from '../patologia.service';
import { FichaService } from '../ficha.service';
import { HistorialService } from '../../historial/historial.service';

@Component({
  selector: 'app-ficha-atencion',
  templateUrl: './ficha-atencion.component.html',
  styleUrls: ['./ficha-atencion.component.css']
})
export class FichaAtencionComponent implements OnInit {

  public usuario: Usuario;
  public ficha: Ficha[];
  public paciente: Paciente[];
  public patologias: Patologia[];
  public formFicha: FormGroup;
  public nombreDoctor: FormControl;
  public nombrePaciente: FormControl;
  public nombrePatologia: FormControl;
  public detalle: FormControl;
  public nombre: string;
  public apellido: string;
  public pacientesNombres: string[];
  public patologiasID: number[];
  public patologiasNombres: any[];

  constructor(private pacienteService: PacienteService,
              private patologiaService: PatologiaService,
              private fichaService: FichaService) { 
    this.paciente = [];
    this.patologias = [];
    this.pacientesNombres = [];
    this.patologiasID = [];
    this.patologiasNombres = [];
    this.ficha = [];
    this.usuario = AutenticacionService.buscarUsuarioEnSesion();
    this.nombre = this.usuario[0].nombre;
    this.apellido = this.usuario[0].apellido;
    this.formFicha = new FormGroup({
      nombreDoctor : new FormControl({value: '', disabled: true}),
      nombrePaciente : new FormControl('',Validators.required),
      nombrePatologia : new FormControl('', Validators.required),
      detalle: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.pacienteService.buscarPacientes().subscribe(res =>{
      this.paciente = res;
      //console.log(this.paciente);
      for (let paciente of this.paciente) {
        this.pacientesNombres.push(paciente.nombre + " " + paciente.apellidoPaterno);
      }
      this.formFicha.controls.nombreDoctor.setValue(this.nombre + " " + this.apellido);
      //console.log(this.pacientesNombres);
    this.patologiaService.buscarPatologias().subscribe(res => {
      this.patologias = res;
      for (let patologia of this.patologias) {
        this.patologiasID.push(patologia.id);
        this.patologiasNombres.push(patologia.nombrePatologia);
      }
      //console.log(this.patologiasID);
      //console.log(this.patologiasNombres);
    });
    });

  }

  agregarPatologiaALista(patologia) {
    this.patologiasNombres.push(patologia);
    //console.log(this.patologiasNombres);
  }

  crearFicha() {
    if(this.formFicha.valid) {
      let ficha = new Ficha();
      ficha.nombreDoctor = this.formFicha.controls.nombreDoctor.value;
      ficha.nombrePaciente = this.formFicha.controls.nombrePaciente.value;
      ficha.patologia = this.formFicha.controls.nombrePatologia.value;
      ficha.detalle = this.formFicha.controls.detalle.value;
      this.ficha.push(ficha);
      //console.log(this.ficha);
    }
    HistorialService.guardarFichaEnSesion(this.ficha);
    this.formFicha.controls.nombrePaciente.reset();
    this.formFicha.controls.nombrePatologia.reset();
    this.formFicha.controls.detalle.reset();
  }

}
