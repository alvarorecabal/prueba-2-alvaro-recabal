import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearPatologiaComponent } from './crear-patologia.component';

describe('CrearPatologiaComponent', () => {
  let component: CrearPatologiaComponent;
  let fixture: ComponentFixture<CrearPatologiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearPatologiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearPatologiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
