import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Patologia } from '../patologia';
import { PatologiaService } from '../patologia.service';

@Component({
  selector: 'app-crear-patologia',
  templateUrl: './crear-patologia.component.html',
  styleUrls: ['./crear-patologia.component.css']
})
export class CrearPatologiaComponent implements OnInit {

  @Output()
  public emisorEventoPatologia: EventEmitter<String>;
  public formPatologia: FormGroup;
  public patologia: Patologia;
  public nombrePatologia: FormControl;

  constructor(private patologiaService: PatologiaService) {
    this.patologia = new Patologia();
    this.emisorEventoPatologia = new EventEmitter<String>();
    this.nombrePatologia = new FormControl('',Validators.required);
   }

  ngOnInit() {
    this.formPatologia = new FormGroup({
      nombrePatologia : this.nombrePatologia
    });
  }

  CrearPatologia(){
    if(this.formPatologia.valid) {
      this.patologia.nombrePatologia = this.nombrePatologia.value;
      this.patologiaService.guardarPatologia(this.patologia).subscribe(res => {
        this.patologia = res;
        this.emisorEventoPatologia.emit(this.patologia.nombrePatologia);
        this.formPatologia.reset();
      });
    }
  }

}
