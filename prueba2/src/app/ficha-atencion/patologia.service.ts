import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Patologia } from './patologia';

@Injectable()
export class PatologiaService {

  private URL_API = 'http://localhost:3004/patologias';

  constructor(private httpClient: HttpClient) { }

  buscarPatologias() : Observable<Patologia[]> {
    return this.httpClient.get<Patologia[]>(this.URL_API);
  }

  buscarPatologia(id : Number) : Observable<Patologia> {
    return this.httpClient.get<Patologia>(this.URL_API + '/' + id);
  }

  guardarPatologia(patologia : Patologia) : Observable<Patologia> {
    return this.httpClient.post<Patologia>(this.URL_API, patologia);
  }

}
