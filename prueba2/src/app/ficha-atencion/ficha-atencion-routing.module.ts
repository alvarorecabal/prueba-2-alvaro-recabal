import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FichaAtencionComponent } from './ficha-atencion/ficha-atencion.component';
import { AutenticacionGuard } from '../autenticacion/autenticacion.guard';

const routes: Routes = [
  {path:'crear-ficha', component:FichaAtencionComponent, canActivate:[AutenticacionGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FichaAtencionRoutingModule { }
